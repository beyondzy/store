package com.cy.store.mapper;

import com.cy.store.entity.User;
import com.cy.store.service.ICartService;
import com.cy.store.vo.CartVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserMpperTests {
    @Autowired
    private UserMapper userMapper;
   @Autowired
   private ICartService cartService;
    @Test
    public void insert(){
        User user = new User();
        user.setUsername("小明");
        user.setPassword("024400");
        userMapper.insert(user);
    }
    @Test
    public void find(){
        User user = userMapper.findByName("小明");
        System.out.println(user);
    }

    /**
     * 更新密码的测试
     */
    @Test
    public void updatePassword(){
        userMapper.updatePasswordByUid(6,"123","root",new Date());
    }

    @Test
    public void findByUid(){
        User byUid = userMapper.findByUid(7);
        System.out.println(byUid);
    }
    @Test
    public void updateInfo(){
        User user = new User();
        user.setUid(6);
        user.setPhone("1599233");
        user.setEmail("251515@qq.com");
        user.setGender(1);
        userMapper.updateInfoByUid(user);
        System.out.println("更新成功");
    }
    @Test
    public void updateAvatarByUid(){
        userMapper.updateAvatarByUid(1,"/asd/qwrr","root",new Date());
        System.out.println("更新成功");
    }
    @Test
    public void getVOByCids() {
        Integer[] cids = {1, 2, 6, 7, 8, 9, 10};
        Integer uid = 31;
        List<CartVO> list = cartService.getVOByCids(uid, cids);
        System.out.println("count=" + list.size());
        for (CartVO item : list) {
            System.out.println(item);
        }
    }
}
