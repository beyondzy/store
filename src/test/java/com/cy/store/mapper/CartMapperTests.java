package com.cy.store.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CartMapperTests {
    @Autowired
    CartMapper cartMapper;
    @Test
    public void testDelete(){
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        Integer integer = cartMapper.deleteByCid(list);
        System.out.println(integer);
    }
}
