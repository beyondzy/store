package com.cy.store.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ClassUtils;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FileTests {
    @Test
    public void tesnt1(){
        System.out.println("打印项目的文件路径");
        System.out.println(System.getProperty("user.dir"));;
        //打印classes目录的绝对路径
        String path1 = ClassUtils.getDefaultClassLoader().getResource("").getPath();
        System.out.println("打印classes目录的绝对路径");
        System.out.println(path1);
        try {
            String path2= ResourceUtils.getURL("resources:").getPath();
            System.out.println("打印classes目录的绝对路径");
            System.out.println(path2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
//        System.out.println("static/images/upload/目录的绝对路径");
//        File upload = new File(path,"static/images/upload/");
//        System.out.println(upload.getAbsolutePath());
//        if(!upload.exists()) upload.mkdirs();

    }
}
