package com.cy.store.service;

import com.cy.store.entity.Address;
import com.cy.store.service.ex.ServcieException;
import com.cy.store.service.impl.AddressServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AddSericeTests {
    @Autowired
    AddressServiceImpl addressService;
    @Test
    public void InsertAddress(){
        Address address = new Address();
        address.setName("张三");
        address.setPhone("17858805555");
        address.setAddress("雁塔区小寨华旗");
        try{
            addressService.addNewAddress(11,"小明",address);
        }catch (ServcieException e){
            System.out.println(e.getMessage());
        }
    }
}
