package com.cy.store.service;

import com.cy.store.mapper.DistrictMapper;
import com.cy.store.service.ex.ServcieException;
import com.cy.store.service.impl.DistrictServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DistrictMapperTests {
    @Autowired
    IDistrictService districtService;
    @Test
    public void getNameByCode() {
        try {
            String code = "430000";
            String result = districtService.getNameByCode(code);
            System.out.println(result);
        } catch (ServcieException e) {
            System.out.println(e.getClass().getSimpleName());
            System.out.println(e.getMessage());
        }
    }
}
