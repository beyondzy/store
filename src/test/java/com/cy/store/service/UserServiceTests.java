package com.cy.store.service;

import com.cy.store.entity.User;
import com.cy.store.service.ex.ServcieException;
import com.cy.store.service.impl.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTests {
    @Autowired
    private UserServiceImpl service;
    @Test
    public void reg() {
        try {
            User user = new User();
            user.setUsername("lower9");
            user.setPassword("123456");
            user.setGender(1);
            user.setPhone("17858802974");
            user.setEmail("lower@tedu.cn");
            user.setAvatar("xxxx");
            service.reg(user);
            System.out.println("注册成功！");
        } catch (ServcieException e) {
            System.out.println("注册失败！" + e.getClass().getSimpleName());
            System.out.println(e.getMessage());
        }
    }
    @Test
    public void login(){
        try {
            User user = service.login("lower9", "123456");
            System.out.println("登入成功");
        }catch (ServcieException e){
            System.out.println(e.getMessage());
            System.out.println(e.getClass().getSimpleName());
        }
    }
    @Test
    public void updatePassword(){
        try {
            service.changePassword(71,"lowre","newpassword","newpassword");
            System.out.println("修改密码成功");
        }catch (ServcieException e){
            System.out.println(e.getMessage());
            System.out.println(e.getClass().getSimpleName());
        }
    }
    @Test
    public void getByUid(){
        try {
            User user = service.getByUid(1);
            System.out.println(user.getIsDelete());
        }catch (Exception e){
            System.out.println(e.getMessage());
            System.out.println(e.getClass().getSimpleName());
        }
    }
    @Test
    public void updateInfo(){
        try {
            User user = new User();
            user.setPhone("155562");
            user.setEmail("1223@qq.ocm");
            user.setGender(1);
            service.changeInfo(9,"lower9",user);
            System.out.println("更新成功");
        }catch (ServcieException e){
            System.out.println(e.getMessage());
            System.out.println(e.getClass().getSimpleName());
        }
    }
    @Test
    public void updateavatar(){
        try{
            service.changeAvatar(1,"小明","/assdd/aappp");
            System.out.println("更新成功");
        }catch (ServcieException e){
            System.out.println(e.getClass().getSimpleName());
            System.out.println(e.getMessage());
        }
    }
}
