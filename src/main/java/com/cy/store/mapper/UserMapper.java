package com.cy.store.mapper;

import com.cy.store.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface UserMapper {

    /**
     * 插入用户数据
     * @param user 用户数据
     * @return 受到影响的行数，并以此来判断插入是否成功
     */
    Integer insert(User user);

    /**
     * 根据用户名查询用户
     * @param name 用户名
     * @return 匹配的用户数据，如果没有匹配到，返回0
     */
    User findByName(String name);

    /**
     * 根据UID更新密码
     * @param uid UID
     * @param password 新密码
     * @param modifiedUser 最后修改的执行人
     * @param modifiedTime 修改时间
     * @return 受到影响的行数
     */
    Integer updatePasswordByUid(@Param("uid") Integer uid,
                                @Param("password") String password,
                                @Param("modifiedUser") String modifiedUser,
                                @Param("modifiedTime") Date modifiedTime);

    /**
     * 根据UID查询用户信息
     * @param uid UID
     * @return 匹配的用户信息，如果没有返回null
     */
    User findByUid(@Param("uid")Integer uid);

    /**
     * 根据uid查询用户
     * @param uid 用户uid
     * @return
     */
    User getByUid(@Param("uid") Integer uid);


    /**
     * 更新用户信息
     * @param user 封装新的用户信息
     * @return 受影响的行数
     */
    Integer updateInfoByUid(User user);
    /**
     * 根据uid更新用户的头像
     * @param uid 用户的id
     * @param avatar 新头像的路径
     * @param modifiedUser 修改执行人
     * @param modifiedTime 修改时间
     * @return 受影响的行数
     */
    Integer updateAvatarByUid(
            @Param("uid") Integer uid,
            @Param("avatar") String avatar,
            @Param("modifiedUser") String modifiedUser,
            @Param("modifiedTime") Date modifiedTime);
}
