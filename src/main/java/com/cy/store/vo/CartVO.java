package com.cy.store.vo;

import com.cy.store.entity.BaseEntity;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
@Data
@Component
public class CartVO extends BaseEntity implements Serializable {
    //购物车id
    private Integer cid;
    //用户id
    private Integer uid;
    //商品id
    private Integer pid;
    private Long price;
    private Integer num;
    private String title;
    private Long realPrice;
    private String image;
}
