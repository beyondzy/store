package com.cy.store.service.impl;

import com.cy.store.entity.User;
import com.cy.store.mapper.UserMapper;
import com.cy.store.service.IuserService;
import com.cy.store.service.ex.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.UUID;
@Service
public class UserServiceImpl implements IuserService {
    @Autowired
    private UserMapper userMapper;

    /**
     * 注册
     * @param user 用户信息
     */
    @Override
    public void reg(User user) {
        String username = user.getUsername();
        User result = userMapper.findByName(username);
        if(result!=null){
            throw new UsernameDuplicateException("尝试注册的用户名[" + username + "]已经被占用");
        }
        Date now = new Date();
        String salt = UUID.randomUUID().toString().toUpperCase();
        String md5Password = getMd5Password(user.getPassword(), salt);
        user.setPassword(md5Password);
        user.setSalt(salt);
        user.setIsDelete(0);
        user.setCreatedUser(username);
        user.setCreatedTime(now);
        user.setModifiedUser(username);
        user.setModifiedTime(now);
        //rows返回受到影响的行数
        Integer rows = userMapper.insert(user);
        if(rows!=1){
            throw new InsertException("添加用户数据出现未知错误，请联系系统管理员");
        }
    }

    /**
     * 用户登入
     * @param username 用户名
     * @param password 用户密码
     * @return 返回用户信息
     */
    @Override
    public User login(String username, String password) {
        //根据username查询用户信息
        User result = userMapper.findByName(username);
        //判断查询结果是否为null
        if(result==null){
            throw new UserNotFoundException("用户数据不存在的错误");
        }
        if(result.getIsDelete()==1){
            throw new UserNotFoundException("用户数据不存在的错误");
        }
        String salt = result.getSalt();
        String md5Password = getMd5Password(password, salt);
        if(!result.getPassword().equals(md5Password)){
            throw new PasswordNotMatchException("密码验证错误");
        }
        User user = new User();
        user.setUid(result.getUid());
        user.setUsername(result.getUsername());
        user.setAvatar(result.getAvatar());
        return user;
    }

    /**
     * 更新用户密码
     * @param uid UID
     * @param username 用户名
     * @param oldPassword 老密码
     * @param newPassword 新密码
     */
    @Override
    public void changePassword(Integer uid, String username, String oldPassword, String newPassword) {
        User result = userMapper.findByUid(uid);
        if(result==null){
            throw new UserNotFoundException("用户数据不存在");
        }
        String salt= result.getSalt();
        String md5Password = getMd5Password(oldPassword, salt);
        if(!result.getPassword().contentEquals(md5Password)){
            throw new PasswordNotMatchException("原密码错误");
        }
        String newMd5Password = getMd5Password(newPassword, salt);
        Date now = new Date();
        Integer rows = userMapper.updatePasswordByUid(uid, newMd5Password, username, now);
        if(rows!=1){
            throw new UpdateException("更新用户数据时出现未知错误，请联系系统管理员");
        }
    }

    /**
     * 根据uid查询用户信息
     * @param uid 当前登录的用户的id
     * @return 封装username/phone/email/gender信息的用户
     */
    @Override
    public User getByUid(Integer uid) {
        // 调用userMapper的findByUid()方法，根据参数uid查询用户数据
        User result = userMapper.findByUid(uid);
        // 判断查询结果是否为null
        // 是：抛出UserNotFoundException异常
        if(result==null){
            throw new UserNotFoundException("用户信息不存在");
        }
        // 判断查询结果中的isDelete是否为1
        // 是：抛出UserNotFoundException异常
        if(result.getIsDelete()==1){
            throw new UserNotFoundException("用户信息不存在");
        }
        // 创建新的User对象
        User user = new User();
        // 将以上查询结果中的username/phone/email/gender封装到新User对象中
        user.setUsername(result.getUsername());
        user.setPhone(result.getPhone());
        user.setEmail(result.getEmail());
        user.setGender(result.getGender());
        // 返回新的User对象
        return user;
    }

    /**
     * 修改用户信息
     * @param uid 当前登录的用户的id
     * @param username 当前登录的用户名
     * @param user 用户的新的数据
     */
    @Override
    public void changeInfo(Integer uid, String username, User user) {
        // 调用userMapper的findByUid()方法，根据参数uid查询用户数据
        User result = userMapper.findByUid(uid);
        // 判断查询结果是否为null
        if (result == null) {
            // 是：抛出UserNotFoundException异常
            throw new UserNotFoundException("用户数据不存在");
        }
        // 判断查询结果中的isDelete是否为1
        if(result.getIsDelete()==1){
            throw new UserNotFoundException("用户数据不存在的错误");
        }
        // 向参数user中补全数据：uid
        user.setUid(uid);
        // 向参数user中补全数据：modifiedUser(username)
        user.setModifiedUser(username);
        // 向参数user中补全数据：modifiedTime(new Date())
        user.setModifiedTime(new Date());
        // 调用userMapper的updateInfoByUid(User user)方法执行修改，并获取返回值
        Integer rows = userMapper.updateInfoByUid(user);
        // 判断以上返回的受影响行数是否不为1
        if (rows != 1) {
            // 是：抛出UpdateException异常
            throw new UpdateException("更新用户数据时出现未知错误，请联系系统管理员");
        }
    }

    /**
     * 业务层方法
     * @param uid 当前登录的用户的id
     * @param username 当前登录的用户名
     * @param avatar 用户的新头像的路径
     */
    @Override
    public void changeAvatar(Integer uid, String username, String avatar) {
        User result = userMapper.getByUid(uid);
        if(result==null||result.getIsDelete().equals(1)){
            throw new UserNotFoundException("用户数据不存在");
        }
        Date now = new Date();
        Integer rows = userMapper.updateAvatarByUid(uid, avatar, username, now);
        if(rows!=1){
            throw new UpdateException("更新用户信息时出现未知错误，请联系管理员");
        }
    }


    private String  getMd5Password(String password,String salt){
        for (int i = 0; i < 3; i++) {
            password = DigestUtils.md5DigestAsHex((salt + password + salt).getBytes()).toUpperCase();
        }
        return password;
    }
}
