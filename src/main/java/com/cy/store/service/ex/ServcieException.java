package com.cy.store.service.ex;

public class ServcieException extends RuntimeException{
    public ServcieException() {
        super();
    }

    public ServcieException(String message) {
        super(message);
    }

    public ServcieException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServcieException(Throwable cause) {
        super(cause);
    }

    protected ServcieException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
